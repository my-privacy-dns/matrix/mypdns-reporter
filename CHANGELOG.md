CHANGELOG

Formatting style

-------
Date dd. month Year
Files involed
  - Change made...
-------

Date 11. Aug 2022
/README.md
  - Replaced <details> (unfold) with real chapter
  - Added TOC, in compliance with the removal of <unfold>
  - Merged the cat table to match the merge of Porn Records into Matrix
  - Added terminal line breaks at 72 to 80 chars per line where reasonable 
    possible. (To improve readability on default *nix console)
